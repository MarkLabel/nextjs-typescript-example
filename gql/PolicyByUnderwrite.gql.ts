import { gql } from '@apollo/client'

export const gqlPolicyByUnderwrite = gql`
  query PolicyByUnderwrite($underwriteId: Int!) {
    policyByUnderwrite(underwriteId: $underwriteId) {
      policies {
        coverageTokenId
        expiryTimestamp
        policyId
        issueTimestamp
        multiplier
        name
        eventName
        canClaim
        openInterest
        policyUri
        premiumTokenId
        settlementToken
        settlementTokenAddress
        policyAddress
        underwriteAddress
        underwriteId
        underwriteUri
      }
      underwriteId
    }
  }
`
