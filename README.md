# version yarn & node support

```
set yarn version 4.xx^ 
set node version 18.xx^^
```

# install yarn

```
yarn install
```

# setting env

```
create file .env
copy 

NEXT_PUBLIC_PORT=3000
NEXT_PUBLIC_REQUEST_TIMEOUT=10000

NEXT_PUBLIC_JWT_EXPIRATION= 5m
NEXT_PUBLIC_JWT_SECRET= dd5f3089-40c3-403d-af14-d0c228b05cb4
NEXT_PUBLIC_JWT_REFRESH_TOKEN_SECRET= 7c4c1c50-3230-45bf-9eae-c9b2e401c767

NEXT_PUBLIC_API_BASE_URL=http://localhost:8081 //port api endpoint


```


# run 

```
yarn dev

```
