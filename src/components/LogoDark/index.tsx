const Logo = () => {
  const logoStyles = {
    width: '150px',
    height: '30px'
  }
  return <img src='/images/logo-outsource-dark.png' style={logoStyles} />
}

export default Logo
