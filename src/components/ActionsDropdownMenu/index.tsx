import * as React from 'react'
import IconButton from '@mui/material/IconButton'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { DotsVertical } from 'mdi-material-ui'
import { isEmpty } from 'lodash'
type Props = {
  options: Option[]
}
export type Option = {
  title: string | React.ReactElement
  icon?: React.ReactElement
  callBack?: () => void
  closeWhenClicked: boolean
  disabled?: boolean
  hidden?: boolean
}

const ITEM_HEIGHT = 48

export default function ActionsDropdownMenu({ options }: Props) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <div>
      <IconButton
        aria-label='more'
        id='action-button'
        aria-controls={open ? 'action-menu' : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup='true'
        onClick={handleClick}
      >
        <DotsVertical />
      </IconButton>
      <Menu
        id='action-menu'
        MenuListProps={{
          'aria-labelledby': 'action-button'
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            maxHeight: ITEM_HEIGHT * 6,
            width: '20ch'
          }
        }}
      >
        {options?.map(
          option =>
            !option.hidden && (
              <MenuItem
                key={`action_dropdown_menu_${option.title}`}
                onClick={() => {
                  if (option.callBack) option.callBack()
                  if (option.closeWhenClicked) handleClose()
                }}
                disabled={option.disabled}
                sx={{
                  textTransform: 'capitalize'
                }}
              >
                {option?.icon}
                {option.title}
              </MenuItem>
            )
        )}
      </Menu>
    </div>
  )
}
