import { Grid, Skeleton, Box, Typography } from '@mui/material'

const TablesPreload = () => {
  return (
    <Box sx={{ p: 6 }}>
      <Typography variant='h3'>
        <Skeleton sx={{ mb: 2 }} animation='wave' />
      </Typography>
      <Grid container spacing={5}>
        <Grid item xs={2}>
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
        </Grid>
        <Grid item xs={2}>
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
        </Grid>
        <Grid item xs={2}>
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
        </Grid>
        <Grid item xs={2}>
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
        </Grid>
        <Grid item xs={2}>
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
        </Grid>
        <Grid item xs={2}>
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
          <Skeleton sx={{ mb: 2 }} animation='wave' />
        </Grid>
      </Grid>
    </Box>
  )
}

export default TablesPreload
