const Logo = () => {
  const logoStyles = {
    width: '120px',
    height: '15px',
    marginTop: '0.3rem',
    marginLeft: '-0.2rem'
  }
  return <img src='/images/logo-right.png' style={logoStyles} />
}

export default Logo
