const Logo = () => {
  const logoStyles = {
    width: '200px',
    height: '40px'
  }
  return <img src='/images/logo-outsource.png' style={logoStyles} />
}

export default Logo
