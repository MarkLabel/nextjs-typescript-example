import { Grid, Skeleton, Box, Typography } from '@mui/material'

import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import { AvatarProps } from '@mui/material/Avatar'
import CardContent from '@mui/material/CardContent'
import CustomAvatar from 'src/@core/components/mui/avatar'
import Button from '@mui/material/Button'
const MediumTab = () => {
  const Avatar = styled(CustomAvatar)<AvatarProps>(({ theme }) => ({
    width: 40,
    height: 40,
    marginRight: theme.spacing(4)
  }))

  return (
    <Box>
      <Grid container spacing={6}>
        <Grid item xs={12} md={2.4} sm={4}>
          <Card sx={{ maxHeight: '300px' }}>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}
            >
              <Avatar
                sx={{
                  width: '100px',
                  height: '100px',
                  marginLeft: '1rem',
                  backgroundColor: 'rgba(76, 78, 100, 0.11)'
                }}
                src=''
              />
              <Box sx={{ textAlign: 'center', marginTop: '12px' }}>
                <Typography sx={{ mb: 2 }}></Typography>
              </Box>

              <Box sx={{ textAlign: 'center' }}>
                <Typography variant='body2' sx={{ mb: 3 }}></Typography>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Button
                  size='large'
                  sx={{
                    width: '120px',
                    height: '35px',
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                  type='submit'
                  variant='contained'
                >
                  <Typography
                    variant='inherit'
                    sx={{ textTransform: 'none' }}
                  ></Typography>
                </Button>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={2.4} sm={4}>
          <Card sx={{ maxHeight: '300px' }}>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}
            >
              <Avatar
                sx={{
                  width: '100px',
                  height: '100px',
                  marginLeft: '1rem',
                  backgroundColor: 'rgba(76, 78, 100, 0.11)'
                }}
                src=''
              />
              <Box sx={{ textAlign: 'center', marginTop: '12px' }}>
                <Typography sx={{ mb: 2 }}></Typography>
              </Box>

              <Box sx={{ textAlign: 'center' }}>
                <Typography variant='body2' sx={{ mb: 3 }}></Typography>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Button
                  size='large'
                  sx={{
                    width: '120px',
                    height: '35px',
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                  type='submit'
                  variant='contained'
                >
                  <Typography
                    variant='inherit'
                    sx={{ textTransform: 'none' }}
                  ></Typography>
                </Button>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={2.4} sm={4}>
          <Card sx={{ maxHeight: '300px' }}>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}
            >
              <Avatar
                sx={{
                  width: '100px',
                  height: '100px',
                  marginLeft: '1rem',
                  backgroundColor: 'rgba(76, 78, 100, 0.11)'
                }}
                src=''
              />
              <Box sx={{ textAlign: 'center', marginTop: '12px' }}>
                <Typography sx={{ mb: 2 }}></Typography>
              </Box>

              <Box sx={{ textAlign: 'center' }}>
                <Typography variant='body2' sx={{ mb: 3 }}></Typography>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Button
                  size='large'
                  sx={{
                    width: '120px',
                    height: '35px',
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                  type='submit'
                  variant='contained'
                >
                  <Typography
                    variant='inherit'
                    sx={{ textTransform: 'none' }}
                  ></Typography>
                </Button>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={2.4} sm={4}>
          <Card sx={{ maxHeight: '300px' }}>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}
            >
              <Avatar
                sx={{
                  width: '100px',
                  height: '100px',
                  marginLeft: '1rem',
                  backgroundColor: 'rgba(76, 78, 100, 0.11)'
                }}
                src=''
              />
              <Box sx={{ textAlign: 'center', marginTop: '12px' }}>
                <Typography sx={{ mb: 2 }}></Typography>
              </Box>

              <Box sx={{ textAlign: 'center' }}>
                <Typography variant='body2' sx={{ mb: 3 }}></Typography>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Button
                  size='large'
                  sx={{
                    width: '120px',
                    height: '35px',
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                  type='submit'
                  variant='contained'
                >
                  <Typography
                    variant='inherit'
                    sx={{ textTransform: 'none' }}
                  ></Typography>
                </Button>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={2.4} sm={4}>
          <Card sx={{ maxHeight: '300px' }}>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
              }}
            >
              <Avatar
                sx={{
                  width: '100px',
                  height: '100px',
                  marginLeft: '1rem',
                  backgroundColor: 'rgba(76, 78, 100, 0.11)'
                }}
                src=''
              />
              <Box sx={{ textAlign: 'center', marginTop: '12px' }}>
                <Typography sx={{ mb: 2 }}></Typography>
              </Box>

              <Box sx={{ textAlign: 'center' }}>
                <Typography variant='body2' sx={{ mb: 3 }}></Typography>
              </Box>

              <Box sx={{ display: 'flex', justifyContent: 'center' }}>
                <Button
                  size='large'
                  sx={{
                    width: '120px',
                    height: '35px',
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                  type='submit'
                  variant='contained'
                >
                  <Typography
                    variant='inherit'
                    sx={{ textTransform: 'none' }}
                  ></Typography>
                </Button>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Box>
  )
}

export default MediumTab
