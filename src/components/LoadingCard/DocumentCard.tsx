import { Grid, Skeleton, Box, Typography } from '@mui/material'

import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import { AvatarProps } from '@mui/material/Avatar'
import CardContent from '@mui/material/CardContent'
import CustomAvatar from 'src/@core/components/mui/avatar'
import Button from '@mui/material/Button'
const DocumentCard = () => {
  const Avatar = styled(CustomAvatar)<AvatarProps>(({ theme }) => ({
    width: 40,
    height: 40,
    marginRight: theme.spacing(4)
  }))

  return (
    <>
      <Grid container spacing={3} className='match-height'>
        <Grid item xs={12} md={4} sm={6} sx={{ height: '266px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  mb: 3,
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              >
                <Box>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                  <Typography variant='body2'></Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  mb: 3,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  padding: '1rem',
                  borderRadius: '0.5rem',
                  backgroundColor: '#F7F8FA'
                }}
              >
                <Grid item xs={12}>
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                </Grid>

                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column'
                  }}
                >
                  <Box sx={{ mb: 2.5, display: 'flex', alignItems: 'center' }}>
                    <Typography
                      variant='body2'
                      sx={{ fontSize: '1rem', fontWeight: '400' }}
                    ></Typography>
                  </Box>
                  <Typography
                    variant='h6'
                    sx={{
                      color: '#FF6C37',
                      fontSize: '2rem',
                      fontWeight: '500'
                    }}
                  ></Typography>
                </Box>

                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'flex-end',
                    flexDirection: 'column'
                  }}
                >
                  <Box sx={{ mb: 2.5, display: 'flex', alignItems: 'center' }}>
                    <Typography
                      sx={{ mr: 1.5, fontSize: '1rem', fontWeight: '400' }}
                      variant='body2'
                    ></Typography>
                  </Box>
                  <Typography
                    variant='h6'
                    sx={{ color: '#FF6C37', fontSize: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  textAlign: 'center',
                  mt: 15
                }}
              >
                <Button
                  fullWidth
                  sx={{
                    height: 40,
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                ></Button>
                <Typography variant='body2'></Typography>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={4} sm={6} sx={{ height: '266px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  mb: 3,
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              >
                <Box>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                  <Typography variant='body2'></Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  mb: 3,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  padding: '1rem',
                  borderRadius: '0.5rem',
                  backgroundColor: '#F7F8FA'
                }}
              >
                <Grid item xs={12}>
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                </Grid>

                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column'
                  }}
                >
                  <Box sx={{ mb: 2.5, display: 'flex', alignItems: 'center' }}>
                    <Typography
                      variant='body2'
                      sx={{ fontSize: '1rem', fontWeight: '400' }}
                    ></Typography>
                  </Box>
                  <Typography
                    variant='h6'
                    sx={{
                      color: '#FF6C37',
                      fontSize: '2rem',
                      fontWeight: '500'
                    }}
                  ></Typography>
                </Box>

                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'flex-end',
                    flexDirection: 'column'
                  }}
                >
                  <Box sx={{ mb: 2.5, display: 'flex', alignItems: 'center' }}>
                    <Typography
                      sx={{ mr: 1.5, fontSize: '1rem', fontWeight: '400' }}
                      variant='body2'
                    ></Typography>
                  </Box>
                  <Typography
                    variant='h6'
                    sx={{ color: '#FF6C37', fontSize: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  textAlign: 'center',
                  mt: 15
                }}
              >
                <Button
                  fullWidth
                  sx={{
                    height: 40,
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                ></Button>
                <Typography variant='body2'></Typography>
              </Box>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={4} sm={6} sx={{ height: '266px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={12}>
                <Skeleton sx={{ mb: 2 }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  mb: 3,
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between'
                }}
              >
                <Box>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                  <Typography variant='body2'></Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  mb: 3,
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  padding: '1rem',
                  borderRadius: '0.5rem',
                  backgroundColor: '#F7F8FA'
                }}
              >
                <Grid item xs={12}>
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                </Grid>

                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column'
                  }}
                >
                  <Box sx={{ mb: 2.5, display: 'flex', alignItems: 'center' }}>
                    <Typography
                      variant='body2'
                      sx={{ fontSize: '1rem', fontWeight: '400' }}
                    ></Typography>
                  </Box>
                  <Typography
                    variant='h6'
                    sx={{
                      color: '#FF6C37',
                      fontSize: '2rem',
                      fontWeight: '500'
                    }}
                  ></Typography>
                </Box>

                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'flex-end',
                    flexDirection: 'column'
                  }}
                >
                  <Box sx={{ mb: 2.5, display: 'flex', alignItems: 'center' }}>
                    <Typography
                      sx={{ mr: 1.5, fontSize: '1rem', fontWeight: '400' }}
                      variant='body2'
                    ></Typography>
                  </Box>
                  <Typography
                    variant='h6'
                    sx={{ color: '#FF6C37', fontSize: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  textAlign: 'center',
                  mt: 15
                }}
              >
                <Button
                  fullWidth
                  sx={{
                    height: 40,
                    backgroundColor: 'rgba(76, 78, 100, 0.11)'
                  }}
                ></Button>
                <Typography variant='body2'></Typography>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}

export default DocumentCard
