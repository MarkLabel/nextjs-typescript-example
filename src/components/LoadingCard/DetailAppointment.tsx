import { Grid, Skeleton, Box, Typography } from '@mui/material'

import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import { AvatarProps } from '@mui/material/Avatar'
import CardContent from '@mui/material/CardContent'
import CustomAvatar from 'src/@core/components/mui/avatar'

const DetailAppointment = () => {
  const Avatar = styled(CustomAvatar)<AvatarProps>(({ theme }) => ({
    width: 40,
    height: 40,
    marginRight: theme.spacing(4)
  }))

  return (
    <>
      <Grid container spacing={3} className='match-height'>
        <Grid item xs={12} sm={6} md={8} lg={8} sx={{ height: '200px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={6}>
                <Skeleton sx={{ height: '3rem' }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between',
                  mb: '0.5rem'
                }}
              ></Box>
              <Grid container spacing={5}>
                <Grid item xs={6}>
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                </Grid>
                <Grid item xs={6}>
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                  <Skeleton sx={{ mb: 2 }} animation='wave' />
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={4} sx={{ height: '200px', mb: 5 }}>
          <Card>
            <CardContent>
              <Grid item xs={6}>
                <Skeleton sx={{ height: '3rem' }} animation='wave' />
              </Grid>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'space-between',
                  mb: '0.5rem'
                }}
              ></Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
              <Grid item xs={12}>
                <Skeleton sx={{ height: '1.7rem' }} animation='wave' />
              </Grid>
              <Box>
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <Typography
                    variant='h6'
                    sx={{ fontWeight: '2rem' }}
                  ></Typography>
                </Box>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  )
}

export default DetailAppointment
