// ** React Imports
import { useState, useEffect } from 'react'

// ** Graphql
import { useLazyQuery, useSubscription } from '@apollo/client'
import { gqlPolicyExchangeFeed } from 'src/gql/PolicyExchangeFeed.gql'
import { gqlPlaceOrderInfoSub } from 'src/gql/PlaceOrderInfoSub.gql'

export const useHome = () => {
  const [policy, setPolicy] = useState<{ policyId: number }>()

  const [loadPolicyExchangeFeed, responsePolicyExchangeFeed] = useLazyQuery(
    gqlPolicyExchangeFeed
  )

  const subscribePlaceOrderInfo = useSubscription(gqlPlaceOrderInfoSub, {
    variables: {
      policyId: policy?.policyId ?? 0
    }
  })

  useEffect(() => {
    loadPolicyExchangeFeed()
  }, [loadPolicyExchangeFeed])

  useEffect(() => {
    if (responsePolicyExchangeFeed?.data !== undefined) {
    }
  }, [responsePolicyExchangeFeed])

  useEffect(() => {
    if (subscribePlaceOrderInfo?.data !== undefined) {
    }
  }, [subscribePlaceOrderInfo])
}
