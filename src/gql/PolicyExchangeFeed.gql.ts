import { gql } from '@apollo/client'

export const gqlPolicyExchangeFeed = gql`
  query PolicyExchangeFeed {
    policyExchangeFeed {
      policies {
        settlementToken
        underwriteId
        underwriteName
        result {
          coverageTokenId
          eventName
          expiryTimestamp
          issueTimestamp
          multiplier
          name
          openInterest
          policyAddress
          policyId
          policyUri
          premiumTokenId
          settlementToken
          settlementTokenAddress
          underwriteAddress
          underwriteId
          underwriteIssuer
          underwriteMinimumApproval
          underwriteOpenInspectTimeGap
          underwriteTotalUnderwriter
          underwriteUri
          latestOrderPrice
          latestOrderType
          latestOrderBlockNumber
        }
      }
    }
  }
`
