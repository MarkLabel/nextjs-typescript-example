import * as React from 'react'
import Skeleton from '@mui/material/Skeleton'
import Stack from '@mui/material/Stack'

export const CanvasLayoutPulse = () => {
  return (
    <Stack spacing={2}>
      <Skeleton variant='rectangular' width={210} height={118} />
      <Skeleton />
      <Skeleton />
      <Skeleton width='60%' />
    </Stack>
  )
}
