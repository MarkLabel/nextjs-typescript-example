import { createContext, useEffect, useState, ReactNode } from 'react'
import { useRouter } from 'next/router'
import http from 'src/libs/axios'
import authConfig from 'src/configs/auth'
import {
  AuthValuesType,
  LoginParams,
  ErrCallbackType,
  IGetUserResponse,
  UpdatePassword
} from './types'

import {
  getAuth,
  signInWithEmailAndPassword,
  signOut,
  updatePassword,
  EmailAuthProvider,
  reauthenticateWithCredential,
  onAuthStateChanged
} from 'firebase/auth'
import toast from 'react-hot-toast'
import app from 'src/configs/firebase'

const defaultProvider: AuthValuesType = {
  user: null,
  loading: true,
  setUser: () => null,
  setLoading: () => Boolean,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  updatePassword: async () => await Promise.resolve(),
  userToken: null
}

const AuthContext = createContext(defaultProvider)

type Props = {
  children: ReactNode
}

const AuthProvider = ({ children }: Props) => {
  const [user, setUser] = useState<IGetUserResponse | null>(
    defaultProvider.user
  )

  const [userToken, setUserToken] = useState<string | null>(
    defaultProvider.userToken
  )
  const [loading, setLoading] = useState<boolean>(defaultProvider.loading)


  const handleLogin = async (
    params: LoginParams,
    errorCallback?: ErrCallbackType
  ) => {

  }

  const handleLogout = () => {

  }

  const handleUpdatePassword = async (
    params: UpdatePassword,
    errorCallback?: ErrCallbackType
  ) => {

  }

  const values = {
    user,
    loading,
    setUser,
    setLoading,
    login: handleLogin,
    logout: handleLogout,
    updatePassword: handleUpdatePassword,
    userToken
  }

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>
}

export { AuthContext, AuthProvider }
