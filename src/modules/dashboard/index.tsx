import Divider from '@mui/material/Divider'
import Box from '@mui/material/Box'
import Rectangle from 'src/components/Rectangle'
import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Button from '@mui/material/Button';
import { useTranslation } from 'react-i18next'
import { useRouter } from 'next/router'

const Dashboard = () => {
  //สำหรับกำหนด route ของ proejct หรือสั่งให้ไปหน้านั้นๆ
  const router = useRouter()
  const getPage = (page: string) => {
    //รับค่าจาก function getPage onclick เพื่อกำหนด route ที่จะไป
    router.push(`/user/${page}`)
  }

  return (
    <>
      <Card>
        <Typography sx={{ p: 5 }}>GET </Typography>
        <Grid container spacing={4} sx={{ p: 5 }}>
          <Grid item xs={2}>
            <Button onClick={() => getPage('getUserAll')} color="success" variant="contained">Get : User All</Button>
          </Grid>
        </Grid>

        <Typography sx={{ p: 5 }}>POST </Typography>
        <Grid container spacing={4} sx={{ p: 5 }}>
          <Grid item xs={4}>
            <Button onClick={() => getPage('getUserById')} color="warning" variant="contained">POST : User By ID</Button>
          </Grid>
          <Grid item xs={4}>
            <Button onClick={() => getPage('getUserPagination')} color="warning" variant="contained">POST : User Pagination</Button>
          </Grid>
          <Grid item xs={4}>
            <Button onClick={() => getPage('getUserSearchPagination')} color="warning" variant="contained">POST : User Search Pagination</Button>
          </Grid>
        </Grid>
        <Grid container spacing={4} sx={{ p: 5 }}>
          <Grid item xs={4}>
            <Button onClick={() => getPage('createUser')} color="warning" variant="contained">POST : User Create</Button>
          </Grid>
        </Grid>

        <Typography sx={{ p: 5 }}>PUT </Typography>
        <Grid container spacing={4} sx={{ p: 5 }}>
          <Grid item xs={4}>
            <Button onClick={() => getPage('updateUser')} color="info" variant="contained">PUT : User Update</Button>
          </Grid>
          <Grid item xs={4}>
            <Button onClick={() => getPage('deleteUser')} color="info" variant="contained">PUT : User Delete</Button>
          </Grid>
        </Grid>

        <Typography sx={{ p: 5 }}>Create , Read , Update , Delete </Typography>
        <Grid container spacing={4} sx={{ p: 5 }}>
          <Grid item xs={4}>
            <Button onClick={() => getPage('createReadUpdateDelete')} variant='contained'
              sx={{
                backgroundColor: 'purple',
                color: '#FFF',
              }} >CRUD</Button>
          </Grid>
        </Grid>
      </Card >
    </>
  )
}

export default Dashboard
