export const apiEndPoint = {
  getUserAll: '/user/all',
  getUserById: 'user/find',
  getUserPagination: 'user/pagination',
  getUserSearchAndPagination: 'user/searchAndPagination',
  createUsers: 'user/create',
  updateUsers: 'user/update',
  deleteUser: 'user/delete'
}

