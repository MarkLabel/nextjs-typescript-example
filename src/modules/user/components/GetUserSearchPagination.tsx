
import { useState, useEffect } from 'react'
import { DataGrid, GridColumns } from '@mui/x-data-grid'
import { Select, MenuItem, Box, Typography, Card, TextField, Button, Grid, CardActions, InputLabel } from '@mui/material';
import { FormInputsSearchPagination } from '../types'
import { useUser } from '../hooks/useUser';
import { setRowDataType } from '../types';
import { useForm, Controller } from 'react-hook-form';


const GetUserSearchPagination = () => {
  //useForm เป็นตัวจัดการ ค่าใน form เช่น
  //control ตัวแปรสำหรับการจัดการ ค่าใน form
  //handleSubmit สำหรับการจัดการ เมื่อกดบันทึกหรือ submit
  //getvalue สำหรับการ ดึงข้อมูลใน form
  //setValue สำหรับการ กำหนดข้อมูลใน form
  //defaultValues ค่าเร่ิมต้นใน form ถ้าใน form มี 3 ค่าต้องประกาศ ให้ครบ
  const { control, handleSubmit, getValues } = useForm<FormInputsSearchPagination>({
    defaultValues: {
      userId: '',
      status: '',
      userRoleId: ''
    },
  });

  //getUserSearchAndPagination ,setPaginationDataUser
  // ของ useUser คือการ import function  จาก hooks เข้ามาใช้งานใน components
  const { getUserSearchAndPagination, setPaginationDataUser } = useUser()
  //gridData และ setRowData คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [gridData, setRowData] = useState<setRowDataType>([])
  //rowCount และ setRowCount คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [rowCount, setRowCount] = useState<number>(0)
  //pageSize และ setPageSize คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [pageSize, setPageSize] = useState<number>(10)
  //page และ setPage คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช
  const [page, setPage] = useState<number>(0)

  //columns ใช้ใน Data grid
  //headerName คือการประกาศ ชื่อที่ในจะใช้เป็น หัวตาราง
  //field ประกาศในตรงกับ ตรง result ที่ setRowData ถ้าไม่รู้ console.log(result) ใน function handleGetUserSearchPagination
  const columns: GridColumns = [
    {
      flex: 0.275,
      minWidth: 400,
      field: 'uuid',
      headerName: 'UserId',
    },
    {
      flex: 0.275,
      minWidth: 200,
      field: 'username',
      headerName: 'Username',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'status',
      headerName: 'Status',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'userRoleId',
      headerName: 'UserRole',
    },
    {
      flex: 0.275,
      minWidth: 300,
      field: 'createdBy',
      headerName: 'CreatedBy',
    },
  ]

  const handleGetUserSearchPagination = async (userId: string, status: string, userRoleId: string) => {
    // //ประกาศเรียก data ที่ function getUserSearchAndPagination ในโฟร์เดอร์ hooks เพื่อ call api
    const data = {
      userId: userId,
      status: userRoleId,
      userRoleId: userRoleId
    }
    const response = await getUserSearchAndPagination(data)
    //เช็คค่าที่ส่งคืนมามีจำนวนมากกว่า 0 ไหม
    //setRowData คือ ส่งไปเก็บใน state ที่ชื่อ gridData ด้านบน
    setRowData(response?.data)
    //setRowCount คือ ส่งไปเก็บใน state ที่ชื่อ rowCount  ด้านบน
    setRowCount(response?.totalData)

  }

  useEffect(() => {
    //เรียกหน้าใหม่ทุกครั้งจะเข้า function นี้
    const userId = getValues('userId')
    const status = getValues('status')
    const userRoleId = getValues('userRoleId')
    // โดย getValues ข้อมูลจาก textfield หรือ select ใน form search
    // แล้วส่งไปที่ function handleGetUserSearchPagination เพื่อส่งค่าไปยัง api
    handleGetUserSearchPagination(userId, status, userRoleId)
  }, [page, pageSize])


  const handlePageSizeChange = async (newPageSize: number) => {
    //handlePageChange เป็นตัวจัดเรื่องของการ กำหนดจำนวนข้อมูล
    //set จำนวนข้อมูลที่แสดง เสมอ
    setPageSize(newPageSize)
    setPage(0)
    //set จำนวนข้อมูลที่แสดง ไปที่ hooks
    await setPaginationDataUser({
      limit: newPageSize,
      offset: page
    })
  }

  const handlePageChange = async (newPage: number) => {
    //handlePageChange เป็นตัวจัดเรื่องของการ เปลี่ยนหน้าในตาราง
    //set หน้าใหม่ที่เปลี่ยนเสมอ
    setPage(newPage)
    //set ส่งหน้าใหม่ที่เปลี่ยนเสมอ ไปที่ hooks
    await setPaginationDataUser({
      limit: pageSize,
      offset: newPage
    })
  }

  const handleSubmitSearchUserPagination = (data: FormInputsSearchPagination) => {
    const userId = data.userId; // ดึง value หรือค่า จาก  Textfield ที่ชื่อ userId
    const status = data.status; // ดึง value หรือค่า จาก Select ที่ชื่อ status แ
    const userRoleId = data.userRoleId; // ดึง value หรือค่า จาก  Select ที่ชื่อ Role
    handleGetUserSearchPagination(userId, status, userRoleId);
  }

  return (
    <>
      <Card>
        <Typography sx={{ p: 5 }}>POST : User Search Pagination</Typography>
        <Box sx={{ p: 5 }}>
          <form onSubmit={handleSubmit(handleSubmitSearchUserPagination)}>
            <Grid container spacing={6} alignItems="center">
              <Grid item xs={12} sm={4}>
                <InputLabel>UserId</InputLabel>
                <Controller
                  name="userId"
                  control={control}
                  render={({ field: { value, onChange } }) => (
                    <TextField
                      value={value}
                      onChange={onChange}
                      fullWidth
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <InputLabel>Status</InputLabel>
                <Controller
                  name="status"
                  control={control}
                  render={({ field: { value, onChange } }) => (
                    <Select
                      fullWidth
                      value={value}
                      onChange={onChange}
                      inputProps={{ placeholder: 'สถานะ' }}
                    >
                      <MenuItem value='ACTIVE'>ACTIVE</MenuItem>
                      <MenuItem value='INACTIVE'>INACTIVE</MenuItem>
                    </Select>
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <InputLabel>userRole</InputLabel>
                <Controller
                  name="userRoleId"
                  control={control}
                  render={({ field: { value, onChange } }) => (
                    <Select
                      fullWidth
                      value={value}
                      onChange={onChange}
                      inputProps={{ placeholder: 'Role' }}
                    >
                      <MenuItem value='ADMIN'>ADMIN</MenuItem>
                      <MenuItem value='USER'>USER</MenuItem>
                    </Select>
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={2}>
                <Button
                  color="warning"
                  variant="contained"
                  type="submit"
                  sx={{ mt: 5, height: '3.5rem' }}
                >
                  ค้นหา
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
        {/* เครื่องมือสำหรับการทำตาราง
        columns รับค่าจากการกำหนด ด้านบน
        rowCount รับค่าจากการ setRowCount  ด้านบน
        page ให้เริ่มต้นที่หน้าไหน default 0
        pageSize default 10
        onPageSizeChange การเปลี่ยนการกำหนดจำนวนที่แสดงข้อมูล ชี้ไปที่ handlePageSizeChange
        onPageChange การเปลี่ยนหน้า ชี้ไปที่ handlePageChange
        pagination default
        paginationMode='server' default
        rows รับค่าจากการ setRowData คือข้อมูลที่จะแสดง
        initialState default
        rowsPerPageOptions กำหนดตัวแสดงจำนวน limit ในตาราง */}
        <DataGrid
          autoHeight
          columns={columns}
          rowCount={rowCount}
          page={page}
          pageSize={pageSize}
          onPageSizeChange={handlePageSizeChange}
          pagination
          paginationMode='server'
          onPageChange={handlePageChange}
          initialState={{
            pagination: {
              page: 1
            }
          }}
          rows={gridData}
          rowsPerPageOptions={[10, 25, 50, 100]}
        />
      </Card>
    </>
  )
}

export default GetUserSearchPagination
