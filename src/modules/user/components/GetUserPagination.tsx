
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useState, useEffect } from 'react'
import { DataGrid, GridColumns } from '@mui/x-data-grid'
import { useUser } from '../hooks/useUser';
import { setRowDataType } from '../types';

const GetUserPagination = () => {
  //getUserPagination ,setPaginationDataUser
  // ของ useUser คือการ import function  จาก hooks เข้ามาใช้งานใน components
  const { getUserPagination, setPaginationDataUser } = useUser()
  //gridData และ setRowData คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [gridData, setRowData] = useState<setRowDataType>([])
  //rowCount และ setRowCount คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [rowCount, setRowCount] = useState<number>(0)
  //pageSize และ setPageSize คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [pageSize, setPageSize] = useState<number>(10)
  //page และ setPage คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช
  const [page, setPage] = useState<number>(0)

  const handleGetUserPagination = async (
  ) => {
    //ประกาศเรียก data ที่ function getUserPagination ในโฟร์เดอร์ hooks
    const response = await getUserPagination()

    //เช็คค่าที่ส่งคืนมามีค่าไหม
    if (response.data) {
      //setRowData คือ ส่งไปเก็บใน state gridData ด้านบน
      setRowData(response.data)
      //setRowCount คือ ส่งไปเก็บใน state rowCount  ด้านบน
      setRowCount(response.totalData)
    }
  }

  //เรียกเข้า functionนี้เสมอ หรือเมื่อมีการ เปลี่ยนแปลงค่าของ page  และ pageSize
  useEffect(() => {
    //สั่งให้ชี้ไปที่ function handleGetUserPagination เพื่อทำต่อ
    handleGetUserPagination()
  }, [page, pageSize])


  const handlePageSizeChange = async (newPageSize: number) => {
    //handlePageChange เป็นตัวจัดเรื่องของการ กำหนดจำนวนข้อมูล
    //set จำนวนข้อมูลที่แสดง เสมอ
    setPageSize(newPageSize)
    setPage(0)
    //set จำนวนข้อมูลที่แสดง ไปที่ hooks
    await setPaginationDataUser({
      limit: newPageSize,
      offset: page
    })
  }

  const handlePageChange = async (newPage: number) => {
    //handlePageChange เป็นตัวจัดเรื่องของการ เปลี่ยนหน้าในตาราง
    //set หน้าใหม่ที่เปลี่ยนเสมอ
    setPage(newPage)
    //set ส่งหน้าใหม่ที่เปลี่ยนเสมอ ไปที่ hooks
    await setPaginationDataUser({
      limit: pageSize,
      offset: newPage
    })
  }

  //columns ใช้ใน Data grid
  //headerName คือการประกาศ ชื่อที่ในจะใช้เป็น หัวตาราง
  //field ประกาศในตรงกับ ตรง result ที่ setRowData ถ้าไม่รู้ console.log(result) ใน function handleGetUserPagination
  const columns: GridColumns = [
    {
      flex: 0.275,
      minWidth: 400,
      field: 'uuid',
      headerName: 'UserId',
    },
    {
      flex: 0.275,
      minWidth: 200,
      field: 'username',
      headerName: 'Username',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'status',
      headerName: 'Status',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'userRoleId',
      headerName: 'UserRole',
    },
    {
      flex: 0.275,
      minWidth: 300,
      field: 'createdBy',
      headerName: 'CreatedBy',
    },
  ]

  return (
    <>
      <Card>
        <Typography sx={{ p: 5 }}>POST : User Pagination</Typography>
        {/* เครื่องมือสำหรับการทำตาราง
        columns รับค่าจากการกำหนด ด้านบน
        rowCount รับค่าจากการ setRowCount  ด้านบน
        page ให้เริ่มต้นที่หน้าไหน default 0
        pageSize default 10
        onPageSizeChange การเปลี่ยนการกำหนดจำนวนที่แสดงข้อมูล ชี้ไปที่ handlePageSizeChange
        onPageChange การเปลี่ยนหน้า ชี้ไปที่ handlePageChange
        pagination default
        paginationMode='server' default
        rows รับค่าจากการ setRowData คือข้อมูลที่จะแสดง
        initialState default
        rowsPerPageOptions กำหนดตัวแสดงจำนวน limit ในตาราง */}
        <DataGrid
          autoHeight
          columns={columns}
          rowCount={rowCount}
          page={page}
          pageSize={pageSize}
          onPageSizeChange={handlePageSizeChange}
          pagination
          paginationMode='server'
          onPageChange={handlePageChange}
          initialState={{
            pagination: {
              page: 1
            }
          }}
          rows={gridData}
          rowsPerPageOptions={[10, 25, 50, 100]}
        />
      </Card>
    </>
  )
}

export default GetUserPagination
