import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Typography,
  Button,
  Card
} from '@mui/material';
import { useState, useEffect } from 'react'
import { DataGrid, GridColumns, GridRenderCellParams } from '@mui/x-data-grid'
import { useUser } from '../hooks/useUser';
import { setRowDataType } from '../types';
import { useRouter } from 'next/router'
import toast from 'react-hot-toast'
const CreateReadUpdateDelete = () => {
  // ของ useUser คือการ import function  จาก hooks เข้ามาใช้งานใน components
  const { getUserPagination, setPaginationDataUser, deleteUser } = useUser()
  //gridData และ setRowData คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [gridData, setRowData] = useState<setRowDataType>([])
  //rowCount และ setRowCount คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [rowCount, setRowCount] = useState<number>(0)
  //pageSize และ setPageSize คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [pageSize, setPageSize] = useState<number>(10)
  //page และ setPage คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช
  const [page, setPage] = useState<number>(0)
  //openCancel และ setOpenCancel คือ useState สำหรับการเปิดปิด diglogs
  const [openCancel, setOpenCancel] = useState<boolean>(false)
  //เก็บค่า uuid ไว้ใน state ที่ชื่อ userId เพื่อนำไปทำการยกเลิก
  const [userId, setUserId] = useState<string>('')
  //สำหรับกำหนด route ของ proejct หรือสั่งให้ไปหน้านั้นๆ
  const router = useRouter()

  const handleGetUserPagination = async (
  ) => {
    //ประกาศเรียก data ที่ function getUserPagination ในโฟร์เดอร์ hooks
    const response = await getUserPagination()
    //เช็คค่าที่ส่งคืนมามีค่าไหม
    if (response.data) {
      //setRowData คือ ส่งไปเก็บใน state gridData ด้านบน
      setRowData(response.data)
      //setRowCount คือ ส่งไปเก็บใน state rowCount  ด้านบน
      setRowCount(response.totalData)
    }
  }

  //handleOpenCancelUser สำหรับ เปิด dialogs setOpenCancel true = เปิด
  const handleOpenCancelUser = () => {
    //สั่งให้ diglogs เปิด โดย setOpenCancel = true(เปิด)
    setOpenCancel(true)
  }

  //handleCloseCancelUser สำหรับ เปิด dialogs setOpenCancel false = ปิด
  const handleCloseCancelUser = () => {
    //สั่งให้ diglogs ปิด โดย setOpenCancel = true(ปิด)
    setOpenCancel(false)
  }

  const handleDeleteUser = async () => {
    //deleteUser call ไปที่ hooks เพื่อทำการลบ user
    const response = await deleteUser(userId);
    if (response === null) {
      toast.error('ยกเลิก userId ไม่สำเร็จ')
    } else {
      toast.success('ยกเลิก userId สำเร็จ')
      //เรียก handleGetUserPagination เพื่อโหลดข้อมูลใหม่
      handleGetUserPagination()
      //handleCloseCancelUser สำหรับปิด diglogs เมื่อยกเลิกเสร็จ
      handleCloseCancelUser()
    }
  }

  //เรียกเข้า functionนี้เสมอ หรือเมื่อมีการ เปลี่ยนแปลงค่าของ page  และ pageSize
  useEffect(() => {
    //สั่งให้ชี้ไปที่ function handleGetUserPagination เพื่อทำต่อ
    handleGetUserPagination()
  }, [page, pageSize])


  const handlePageSizeChange = async (newPageSize: number) => {
    //handlePageChange เป็นตัวจัดเรื่องของการ กำหนดจำนวนข้อมูล
    //set จำนวนข้อมูลที่แสดง เสมอ
    setPageSize(newPageSize)
    setPage(0)
    //set จำนวนข้อมูลที่แสดง ไปที่ hooks
    await setPaginationDataUser({
      limit: newPageSize,
      offset: page
    })
  }

  const handlePageChange = async (newPage: number) => {
    //handlePageChange เป็นตัวจัดเรื่องของการ เปลี่ยนหน้าในตาราง
    //set หน้าใหม่ที่เปลี่ยนเสมอ
    setPage(newPage)
    //set ส่งหน้าใหม่ที่เปลี่ยนเสมอ ไปที่ hooks
    await setPaginationDataUser({
      limit: pageSize,
      offset: newPage
    })
  }

  //columns ใช้ใน Data grid
  //headerName คือการประกาศ ชื่อที่ในจะใช้เป็น หัวตาราง
  //field ประกาศในตรงกับ ตรง result ที่ setRowData ถ้าไม่รู้ console.log(result) ใน function handleGetUserAll
  const columns: GridColumns = [
    {
      flex: 0.275,
      minWidth: 400,
      field: 'uuid',
      headerName: 'UserId',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'username',
      headerName: 'Username',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'status',
      headerName: 'Status',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'userRoleId',
      headerName: 'UserRole',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'createdBy',
      headerName: 'CreatedBy',
    },
    {
      flex: 0.2,
      minWidth: 200,
      field: 'option',
      headerName: 'Option',
      renderCell: (params: GridRenderCellParams) => {
        //ค่าที่แสดงในตารางในแต่ละแถว
        const { row } = params

        //function cancelUser
        const cancelUser = () => {
          //เก็บค่า uuid ไว้ใน state ที่ชื่อ userId เพื่อนำไปทำการยกเลิก
          setUserId(row.uuid)

          //handleOpenCancelUser สำหรับการเปิด Dialogs เพื่อทำการยืนยันการยกเลิก
          handleOpenCancelUser()
        }

        const updateUser = () => {
          //ลิงก์ไปที่ user/updateUser/userId เพื่อทำการ update
          router.push(`/user/updateUser/${row.uuid}`)
        }

        return (
          <>
            <Button onClick={updateUser} color="warning" variant="contained">edit</Button>
            <Button onClick={cancelUser} sx={{ ml: 5 }} color="error" variant="contained">delete</Button>
          </>
        )
      }
    }
  ]

  const createUser = () => {
    //เพื่อกำหนด route ที่จะไป
    router.push(`/user/createUser`)
  }

  return (
    <>

      <Dialog
        open={openCancel}
        onClose={handleCloseCancelUser}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>ยืนยันการยกเลิกข้อมูล User?</DialogTitle>
        <DialogContent>
          <DialogContentText id='alert-dialog-description'>
            คุณต้องการยกเลิก user ที่มี รหัส : {userId}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseCancelUser} variant='outlined' color='secondary'>
            ปิด
          </Button>
          <Button onClick={handleDeleteUser} color="error" variant='contained'>
            ยืนยัน
          </Button>
        </DialogActions>
      </Dialog>
      <Card>
        <Typography sx={{ p: 5 }}>Create , Read , Update , Delete</Typography>

        <Box sx={{ pt: 5, pl: 5, pb: 5 }}>
          <Button color="info" onClick={createUser} variant="contained">Create</Button>
        </Box>
        {/* เครื่องมือสำหรับการทำตาราง
        columns รับค่าจากการกำหนด ด้านบน
        rowCount รับค่าจากการ setRowCount  ด้านบน
        page ให้เริ่มต้นที่หน้าไหน default 0
        pageSize default 10
        onPageSizeChange การเปลี่ยนการกำหนดจำนวนที่แสดงข้อมูล ชี้ไปที่ handlePageSizeChange
        onPageChange การเปลี่ยนหน้า ชี้ไปที่ handlePageChange
        pagination default
        paginationMode='server' default
        rows รับค่าจากการ setRowData คือข้อมูลที่จะแสดง
        initialState default
        rowsPerPageOptions กำหนดตัวแสดงจำนวน limit ในตาราง */}
        <DataGrid
          autoHeight
          columns={columns}
          rowCount={rowCount}
          page={page}
          pageSize={pageSize}
          onPageSizeChange={handlePageSizeChange}
          pagination
          paginationMode='server'
          onPageChange={handlePageChange}
          initialState={{
            pagination: {
              page: 1
            }
          }}
          rows={gridData}
          rowsPerPageOptions={[10, 25, 50, 100]}
        />
      </Card>
    </>
  )
}

export default CreateReadUpdateDelete
