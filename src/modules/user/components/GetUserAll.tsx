
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useState, useEffect } from 'react'
import { DataGrid, GridColumns } from '@mui/x-data-grid'
import { useUser } from '../hooks/useUser';
import { setRowDataType } from '../types';

const GetUserAll = () => {
  //getDataUserAll
  // ของ useUser คือการ import function จาก hooks เข้ามาใช้งานใน components
  const { getDataUserAll } = useUser()
  //gridData และ setRowData คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [gridData, setRowData] = useState<setRowDataType>([])
  //rowCount และ setRowCount คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [rowCount, setRowCount] = useState<number>(0)

  //ให้ทำงานทุกครั้งที่โหลดหน้า
  useEffect(() => {
    //ชี้ไปทำงานที่ function handleGetUserAll ต่อ
    handleGetUserAll()
  }, [])

  const handleGetUserAll = async (
  ) => {
    //ประกาศเรียก data ที่ function getDataUserAll ในโฟร์เดอร์ hooks
    const response = await getDataUserAll()

    //เช็คค่าที่ส่งคืนมามีจำนวนมากกว่า 0 ไหม
    if (response.length > 0) {
      //setRowData คือ ส่งไปเก็บใน state gridData ด้านบน
      setRowData(response)
      //setRowCount คือ ส่งไปเก็บใน state rowCount  ด้านบน
      setRowCount(response.length)
    }
  }

  //columns ใช้ใน Data grid
  //headerName คือการประกาศ ชื่อที่ในจะใช้เป็น หัวตาราง
  //field ประกาศในตรงกับ ตรง result ที่ setRowData ถ้าไม่รู้ console.log(result) ใน function handleGetUserAll
  const columns: GridColumns = [
    {
      flex: 0.275,
      minWidth: 400,
      field: 'uuid',
      headerName: 'UserId',
    },
    {
      flex: 0.275,
      minWidth: 200,
      field: 'username',
      headerName: 'Username',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'status',
      headerName: 'Status',
    },
    {
      flex: 0.275,
      minWidth: 150,
      field: 'userRoleId',
      headerName: 'UserRole',
    },
    {
      flex: 0.275,
      minWidth: 300,
      field: 'createdAt',
      headerName: 'CreatedAt',
    },
    {
      flex: 0.275,
      minWidth: 300,
      field: 'createdBy',
      headerName: 'CreatedBy',
    },
  ]

  return (
    <>
      <Card>
        <Typography sx={{ p: 5 }}>Get : User All</Typography>
        {/* เครื่องมือสำหรับการทำตาราง */}
        {/* columns รับค่าจากการกำหนด ด้านบน
        rowCount รับค่าจากการ setRowCount  ด้านบน
        pageSize default 10
        rows รับค่าจากการ setRowData คือข้อมูลที่จะแสดง
        rowsPerPageOptions กำหนดตัวแสดงจำนวนผลลัพธ์ในตาราง */}
        <DataGrid
          autoHeight
          columns={columns}
          rowCount={rowCount}
          pageSize={10}
          rows={gridData}
          rowsPerPageOptions={[10, 25, 50, 100]}
        />
      </Card>
    </>
  )
}

export default GetUserAll
