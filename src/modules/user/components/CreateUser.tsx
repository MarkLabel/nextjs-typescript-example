import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { FormHelperText, Select, MenuItem, Box, Typography, Card, TextField, Button, Grid, CardActions, InputLabel } from '@mui/material';
import { useUser } from '../hooks/useUser';
import { FormInputsCreateUser } from '../types'
import toast from 'react-hot-toast'
import { useRouter } from 'next/router'

//defaultValues ค่าเร่ิมต้นใน form ถ้าใน form มี 4 ค่าต้องประกาศ ให้ครบ
const defaultValues = {
  username: '',
  password: '',
  userRoleId: '',
  createdBy: '',
}

const CreateUser = () => {
  //useForm เป็นตัวจัดการ ค่าใน form เช่น
  //control ตัวแปรสำหรับการจัดการ ค่าใน form
  //handleSubmit สำหรับการจัดการ เมื่อกดบันทึกหรือ submit
  //reset สำหรับการ ล้างค่า form
  const { control, handleSubmit, formState: { errors }, reset } = useForm<FormInputsCreateUser>({ defaultValues });
  // ของ useUser คือการ import function  จาก hooks เข้ามาใช้งานใน components
  const { createUser } = useUser()
  //สำหรับกำหนด route ของ proejct หรือสั่งให้ไปหน้านั้นๆ
  const router = useRouter()

  const handleCreateUser = async (data: FormInputsCreateUser) => {
    //จัดการ หรือ ดึงข้อมูลจาก createUser ใน hooks
    const response = await createUser(data);
    //เมื่อ api สำเร็จ หรือ return status 200 เท่ากับ สร้างข้อมูลสำเร็จ
    if (response?.status === 200) {
      //toast success คือ alert สำเร็จ ในหน้าจอ
      toast.success('สร้างข้อมูล User สำเร็จ')
      // reset(defaultValues) ล้างค่าข้อมูล และกลับไปเป็นค่าเริ่มต้น
      reset(defaultValues)
      //เพื่อกำหนด route ที่จะไป
      router.push('/user/createReadUpdateDelete')

    } else {
      //เมื่อ api ไม่สำเร็จ หรือ return ไม่เท่ากับ status 200 เท่ากับ สร้างข้อมูลไม่สำเร็จ
      //toast error คือ alert ไม่สำเร็จ ในหน้าจอ
      toast.error('สร้างข้อมูล User ไม่สำเร็จ')
    }
  }

  return (
    <>
      <Card sx={{ p: 5 }}>
        <Typography sx={{ mb: 5 }}>POST : User Create</Typography>
        <form onSubmit={handleSubmit(handleCreateUser)}>
          <Grid container spacing={6} alignItems="center">
            <Grid item xs={12} sm={3}>
              <InputLabel>Username</InputLabel>
              <Controller
                name="username"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    error={Boolean(errors.username)}
                  />
                )}
              />
              {errors.username != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก username'}
                </FormHelperText>
              )}
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputLabel>Password</InputLabel>
              <Controller
                name="password"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    error={Boolean(errors.password)}
                  />
                )}
              />
              {errors.password != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก password'}
                </FormHelperText>
              )}
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputLabel>UserRole</InputLabel>
              <Controller
                name="userRoleId"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Select
                    fullWidth
                    value={value}
                    onChange={onChange}
                    inputProps={{ placeholder: 'Role' }}
                    error={Boolean(errors.userRoleId)}
                  >
                    <MenuItem value='ADMIN'>ADMIN</MenuItem>
                    <MenuItem value='USER'>USER</MenuItem>
                  </Select>
                )}
              />
              {errors.userRoleId != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก userRoleId'}
                </FormHelperText>
              )}
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputLabel>createdBy</InputLabel>
              <Controller
                name="createdBy"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    error={Boolean(errors.createdBy)}
                  />
                )}
              />
              {errors.createdBy != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก createdBy'}
                </FormHelperText>
              )}
            </Grid>
          </Grid>
          <CardActions sx={{ display: 'flex', justifyContent: 'center' }}>
            <Button
              variant='contained'
              size='large'
              type='submit'
            >
              บันทึก
            </Button>
          </CardActions>
        </form>
      </Card>
    </>
  );
};

export default CreateUser;
