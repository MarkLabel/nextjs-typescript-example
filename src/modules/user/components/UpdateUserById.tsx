import React, { useState, useEffect } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { FormHelperText, Select, MenuItem, Box, Typography, Card, TextField, Button, Grid, CardActions, InputLabel } from '@mui/material';
import { useUser } from '../hooks/useUser';
import { FormInputsUpdateUser, UserData } from '../types'
import toast from 'react-hot-toast'
import { useRouter } from 'next/router'
//defaultValues ค่าเร่ิมต้นใน form ถ้าใน form มี 5 ค่าต้องประกาศ ให้ครบ
const defaultValues = {
  userId: '',
  username: '',
  password: '',
  userRoleId: '',
  updatedBy: '',
}

type Props = {
  slug: { id: string }
}
const UpdateUser = ({ slug }: Props) => {
  //useForm เป็นตัวจัดการ ค่าใน form เช่น
  //control ตัวแปรสำหรับการจัดการ ค่าใน form
  //handleSubmit สำหรับการจัดการ เมื่อกดบันทึกหรือ submit
  //getvalue สำหรับการ ดึงข้อมูลใน form
  //setValue สำหรับการ กำหนดข้อมูลใน form
  //defaultValues ค่าเร่ิมต้นใน form ถ้าใน form มี 5 ค่าต้องประกาศ ให้ครบ
  const { control, handleSubmit, formState: { errors }, reset, getValues, setValue } = useForm<FormInputsUpdateUser>({ defaultValues });
  // ของ useUser คือการ import function  จาก hooks เข้ามาใช้งานใน components
  const { updateUser, getDataUserById } = useUser()
  const router = useRouter()
  const handleUpdateUser = async (data: FormInputsUpdateUser) => {
    //จัดการ หรือ ดึงข้อมูลจาก updateUser ใน hooks
    const response = await updateUser(data);

    //เมื่อ api สำเร็จ หรือ return status 200 เท่ากับ อัพเดทข้อมูลสำเร็จ
    if (response?.status === 200) {
      //toast success คือ alert สำเร็จ ในหน้าจอ
      toast.success('อัพเดทข้อมูล User สำเร็จ')
      router.push('/user/createReadUpdateDelete')
      // reset(defaultValues) ล้างค่าข้อมูล และกลับไปเป็นค่าเริ่มต้น
      reset(defaultValues)
    } else {
      //เมื่อ api ไม่สำเร็จ หรือ return ไม่เท่ากับ status 200 เท่ากับ อัพเดทข้อมูลไม่สำเร็จ
      //toast error คือ alert ไม่สำเร็จ ในหน้าจอ
      toast.error('อัพเดทข้อมูล User ไม่สำเร็จ')
    }
  }

  useEffect(() => {
    if (slug.id !== undefined) {
      const userId = slug.id
      handleGetUserById(userId)
    }

  }, [slug.id])

  const handleGetUserById = async (userId: string) => {
    //await getDataUserById คือการส่งค่า userId ไปที่ getDataUserById ใน hooks เพื่อ call api
    const response = await getDataUserById(userId);
    //เมื่อได้ response จะทำการ setUserData เพื่อเก็บข้อมูลไว้ใน useState เพื่อนำไปแสดงผลด้านล่าง

    if (response === null) {
      //toast error คือ alert ไม่สำเร็จ ในหน้าจอ
      toast.error('ไม่พบ userId')
      // reset(defaultValues) ล้างค่าข้อมูล และกลับไปเป็นค่าเริ่มต้น
      reset(defaultValues)
    } else {
      //เมื่อเราได้ reponse จาก api ใน hooks เราจะทำการ setvalue เข้าไปใน form
      //เพื่อแสดงข้อมูลของ user เพื่อทำการแก้ไข
      const data = response
      setValue('userId', data.uuid)
      setValue('username', data.username)
      setValue('password', data.password)
      setValue('userRoleId', data.userRoleId)
    }
  }

  return (
    <>
      <Card sx={{ p: 5 }}>
        <Typography sx={{ mb: 5 }}>PUT : User Update By Id</Typography>
        <form onSubmit={handleSubmit(handleUpdateUser)}>
          <Grid container spacing={6} alignItems="center">
            <Grid item xs={12} sm={3}>
              <InputLabel>UserId</InputLabel>
              <Controller
                name="userId"
                control={control}

                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    disabled
                    error={Boolean(errors.userId)}
                  />
                )}
              />
              {errors.userId != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก userId'}
                </FormHelperText>
              )}
            </Grid>

          </Grid>
          <Grid sx={{ pt: 10 }} container spacing={6} alignItems="center">
            <Grid item xs={12} sm={3}>
              <InputLabel>Username</InputLabel>
              <Controller
                name="username"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    error={Boolean(errors.username)}
                  />
                )}
              />
              {errors.username != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก username'}
                </FormHelperText>
              )}
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputLabel>Password</InputLabel>
              <Controller
                name="password"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    error={Boolean(errors.password)}
                  />
                )}
              />
              {errors.password != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก password'}
                </FormHelperText>
              )}
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputLabel>UserRole</InputLabel>
              <Controller
                name="userRoleId"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Select
                    fullWidth
                    value={value}
                    onChange={onChange}
                    inputProps={{ placeholder: 'Role' }}
                    error={Boolean(errors.userRoleId)}
                  >
                    <MenuItem value='ADMIN'>ADMIN</MenuItem>
                    <MenuItem value='USER'>USER</MenuItem>
                  </Select>
                )}
              />
              {errors.userRoleId != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก userRoleId'}
                </FormHelperText>
              )}
            </Grid>
            <Grid item xs={12} sm={3}>
              <InputLabel>updatedBy</InputLabel>
              <Controller
                name="updatedBy"
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                    error={Boolean(errors.updatedBy)}
                  />
                )}
              />
              {errors.updatedBy != null && (
                <FormHelperText sx={{ color: 'error.main' }}>
                  {'กรุณากรอก updatedBy'}
                </FormHelperText>
              )}
            </Grid>
          </Grid>
          <CardActions sx={{ display: 'flex', justifyContent: 'center' }}>
            <Button
              variant='contained'
              size='large'
              type='submit'
            >
              บันทึก
            </Button>
          </CardActions>
        </form>
      </Card>
    </>
  );
};

export default UpdateUser;
