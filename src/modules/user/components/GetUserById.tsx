import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Box, Typography, Card, TextField, Button, Grid, CardActions, InputLabel } from '@mui/material';
import { useUser } from '../hooks/useUser';
import { UserData, FormInputsGetUserById } from '../types'
import toast from 'react-hot-toast'

const GetUserById = () => {
  //useForm เป็นตัวจัดการ ค่าใน form เช่น
  //control ตัวแปรสำหรับการจัดการ ค่าใน form
  //handleSubmit สำหรับการจัดการ เมื่อกดบันทึกหรือ submit
  //defaultValues ค่าเร่ิมต้นใน form ถ้าใน form มี 1 ค่าต้องประกาศ ให้ครบ
  const { control, handleSubmit } = useForm<FormInputsGetUserById>({
    defaultValues: {
      userId: '',
    },
  });
  //userData และ setUserData คือ useState สำหรับการ เก็บค่า และ เรียกค่าที่เก็บไปใช้
  const [userData, setUserData] = useState<UserData>()
  //getDataUserById ของ useUser คือการ import function  จาก hooks เข้ามาใช้งานใน components
  const { getDataUserById } = useUser()

  //เมื่อกดค้นหา หรือ button ที่เป็น type submit จะเข้าfunction handleGetUseById
  //เพื่อนำ data.userId ใน useform ที่ชื่อ name userId ไปส่งเรียก api getDataUserById ใน hooks
  const handleGetUserById = async (data: FormInputsGetUserById) => {
    const userId = data.userId; // จาก Textfield ที่ชื่อ userId
    const response = await getDataUserById(userId);
    //เมื่อได้ response จะทำการ setUserData เพื่อเก็บข้อมูลไว้ใน useState ที่ชื่อ userData เพื่อนำไปแสดงผลด้านล่าง
    setUserData(response)
    if (response === null) {
      toast.error('ไม่พบ userId')
    }
  }

  return (
    <>
      <Card sx={{ p: 5 }}>
        <Typography sx={{ mb: 5 }}>POST : User By Id</Typography>
        <form onSubmit={handleSubmit(handleGetUserById)}>
          <Grid container spacing={6} alignItems="center">
            <Grid item xs={12} sm={4}>
              <InputLabel>UserId</InputLabel>
              <Controller
                name="userId"
                control={control}
                render={({ field: { value, onChange } }) => (
                  <TextField
                    value={value}
                    onChange={onChange}
                    fullWidth
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <Button
                color="warning"
                variant="contained"
                type="submit"
                sx={{ mt: 5, height: '3.5rem' }}
              >
                ค้นหา
              </Button>
            </Grid>
          </Grid>
        </form>
        <Box sx={{ pt: 5 }}>
          {/* ดึงข้อมูล uuid จาก userData ใน state */}
          <Typography>UserId : {userData?.uuid} </Typography>
        </Box>
        <Box sx={{ pt: 5 }}>
          {/* ดึงข้อมูล username จาก userData ใน state */}
          <Typography>Username : {userData?.username}</Typography>
        </Box>
        <Box sx={{ pt: 5 }}>
          {/* ดึงข้อมูล status จาก userData ใน state */}
          <Typography>Status : {userData?.status}</Typography>
        </Box>
        <Box sx={{ pt: 5 }}>
          {/* ดึงข้อมูล userRoleId จาก userData ใน state */}
          <Typography>UserRole : {userData?.userRoleId}</Typography>
        </Box>
        <Box sx={{ pt: 5 }}>
          {/* ดึงข้อมูล createdAt จาก userData ใน state */}
          <Typography>createdAt : {userData?.createdAt}</Typography>
        </Box>
        <Box sx={{ pt: 5 }}>
          {/* ดึงข้อมูล createdBy จาก userData ใน state */}
          <Typography>createdBy : {userData?.createdBy}</Typography>
        </Box>
      </Card>
    </>
  );
};

export default GetUserById;
