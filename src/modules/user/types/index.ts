export interface RowData {
  userId: string;
  username: string;
}

export interface paganationType {
  limit: number;
  offset: number;
}


export interface UserData {
  uuid: string;
  username: string;
  status: string;
  userRoleId: string;
  createdAt: string;
  createdBy: string;
}

export interface FormInputsCreateUser {
  username: string;
  password: string;
  userRoleId: string;
  createdBy: string;
}

export interface FormInputsUpdateUser extends FormInputsCreateUser {
  userId: string;
  updatedBy: string;
}

export interface FormInputsSearchPagination {
  userId: string;
  status: string;
  userRoleId: string
}
export interface FormInputsGetUserById {
  userId: string;
}

export type setRowDataType = RowData[];
