import http from 'src/libs/axios'
import { apiEndPoint } from 'src/modules/user/constants'
import { useState } from 'react'
import { FormInputsSearchPagination, paganationType, FormInputsGetUserById, FormInputsCreateUser, FormInputsUpdateUser } from '../types'
export const useUser = () => {
  const [paginationDataUser, setPaginationDataUser] =
    useState<paganationType>({
      limit: 10,
      offset: 0
    })
  const getDataUserAll = async () => {
    try {
      const response = await http.get(
        `${apiEndPoint.getUserAll}`,
      )
      const data = response?.data
      return data
    } catch (error) {
      return []
    }
  }

  const getDataUserById = async (userId: string) => {
    try {
      const response = await http.post(
        `${apiEndPoint.getUserById}`,
        {
          userId: userId
        },
      )

      const data = response?.data
      return data
    } catch (error) {
      return null
    }
  }

  const getUserPagination = async () => {
    try {
      const response = await http.post(
        `${apiEndPoint.getUserPagination}`,
        {
          limit: paginationDataUser.limit || 10,
          offset: paginationDataUser.offset || 0,
        },
      )

      const data = response?.data
      return data
    } catch (error) {
      return []
    }
  }

  const getUserSearchAndPagination = async (args: FormInputsSearchPagination) => {
    try {
      const response = await http.post(
        `${apiEndPoint.getUserSearchAndPagination}`,
        {
          userId: args.userId,
          status: args.status,
          userRoleId: args.userRoleId,
          limit: paginationDataUser.limit || 10,
          offset: paginationDataUser.offset || 0,
        },
      )

      const data = response?.data
      return data
    } catch (error) {
      return []
    }
  }

  const createUser = async (args: FormInputsCreateUser) => {
    try {
      const response = await http.post(
        `${apiEndPoint.createUsers}`,
        {
          ...args
        },
      )
      return response
    } catch (error) {
      return null
    }
  }

  const updateUser = async (args: FormInputsUpdateUser) => {
    try {
      const response = await http.put(
        `${apiEndPoint.updateUsers}`,
        {
          ...args
        },
      )
      return response
    } catch (error) {
      return null
    }
  }

  const deleteUser = async (userId: string) => {
    try {
      const response = await http.put(
        `${apiEndPoint.deleteUser}`,
        {
          userId: userId
        },
      )
      return response
    } catch (error) {
      return null
    }
  }

  return {
    getDataUserAll,
    getDataUserById,
    getUserPagination,
    setPaginationDataUser,
    getUserSearchAndPagination,
    createUser,
    updateUser,
    deleteUser
  }
}
