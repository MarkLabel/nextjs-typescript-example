export default {
  verifyEndpoint: 'user/verifyInviteCode',
  confirmEndpoint: 'user/registerWithInviteCode'
}
