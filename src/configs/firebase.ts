// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyDlmti3rrtzQJdi3c2T89rgcwyWZmV2uTg',
  authDomain: 'gcp-siamraj.firebaseapp.com',
  projectId: 'gcp-siamraj',
  storageBucket: 'gcp-siamraj.appspot.com',
  messagingSenderId: '666162139867',
  appId: '1:666162139867:web:7f15a8b7a4885eb9ccb2fe',
  measurementId: 'G-GBL07TM9BK'
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
// const analytics = getAnalytics(app)
export default app
