// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'
import { useTranslation } from 'react-i18next'
const navigation = (): VerticalNavItemsType => {
  const { t } = useTranslation()

  return [
    {
      sectionTitle: '1'
    },
    {
      title: '111',
      path: '/',
      icon: 'mdi:view-dashboard-outline'
    },

  ]
}

export default navigation
