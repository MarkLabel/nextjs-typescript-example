import UpdateUserByIdModule from 'src/modules/user/components/UpdateUserById'
import { useRouter } from 'next/router'

const UpdateUserByIdPage = () => {
  const router = useRouter()
  const slug = (router.query?.slug as string[]) ?? []
  return (
    <>
      <UpdateUserByIdModule slug={{ id: slug[0] }} />
    </>
  )
}
export default UpdateUserByIdPage
