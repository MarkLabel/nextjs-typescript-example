import Axios from 'axios'

import env from 'src/configs/env'

const httpInstance = Axios.create({
  baseURL: env.API_BASE_URL || 'http://localhost:4000',
  withCredentials: false
})

export default httpInstance
