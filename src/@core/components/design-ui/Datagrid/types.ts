// ** MUI Imports
import {
  DataGrid,
  DataGridProps,
  GridColumns,
  GridRenderCellParams
} from '@mui/x-data-grid'

// ** Types
import { ThemeColor } from 'src/@core/layouts/types'

export type CustomDatagridProps = DataGridProps & {
  color?: ThemeColor
  skin?: 'filled' | 'light' | 'light-static'
}
