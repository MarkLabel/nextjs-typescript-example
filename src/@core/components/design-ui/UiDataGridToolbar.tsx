import React, { ChangeEvent } from 'react'
import router, { useRouter } from 'next/router'
import { t } from 'i18next'
import IconButton from '@mui/material/IconButton'
import { Box, Grid, Button, Breadcrumbs, Link, TextField } from '@mui/material'
import { styled, useTheme } from '@mui/material/styles'
import * as _ from 'lodash'

import { Magnify, Close, Plus } from 'mdi-material-ui'
import ChevronRightIcon from 'src/@core/components/design-ui/icons/ChevronRightIcon'

type Props = {
  breadcrumb: { name: string; path: string; isDisplay: boolean }[]
  toolbarSearchValue: string
  onMutationToolbarSearchValue: (updated: string) => void
  onSubmitToolbarButton: (updated: string) => void
  onChange: (e: ChangeEvent<HTMLInputElement>) => void
  children?: any
}

const UiDataGridToolbar = (props: Props) => {
  const theme = useTheme()

  return (
    <Grid item xs={12}>
      <Grid
        container
        padding={0}
        display='flex'
        flexDirection='row'
        justifyContent='space-between'
        alignItems='center'
        className='relative rounded-lg w-full'
        sx={{
          // backgroundColor: 'white',
          borderRadius: '0.5rem',
          paddingX: '0.3rem',
          pb: '1rem'
        }}
      >
        <Grid item>
          <Breadcrumbs
            maxItems={3}
            aria-label='breadcrumb'
            separator={<ChevronRightIcon color={theme.palette.grey[500]} />}
          >
            <Link
              underline='hover'
              key={`navPathName-home`}
              color={theme.palette.grey[500]}
              href='/dashboard'
            >
              {t(`breadcrumb.Home`) as string}
            </Link>
            {props.breadcrumb?.map((bread, index: number) => (
              <Link
                underline='hover'
                color={
                  index + 1 === props.breadcrumb.length
                    ? theme.palette.primary.main
                    : theme.palette.grey[500]
                }
                key={`navPathName-${index}`}
                href={bread.path}
                onClick={event => {
                  event.preventDefault()
                  router.push(bread.path)
                }}
                sx={{
                  textTransform: 'capitalize'
                }}
              >
                {bread.isDisplay === true
                  ? (t(`breadcrumb.${bread.name}`) as string)
                  : bread.name}
              </Link>
            ))}
          </Breadcrumbs>
        </Grid>
        <Grid item display='flex'>
          <TextField
            size='small'
            sx={{ mr: 2, '& .MuiInputBase-root': { gap: '.5rem' } }}
            onChange={props.onChange}
            placeholder={t('TextField.search') as string}
            value={props.toolbarSearchValue}
            InputProps={{
              startAdornment: <Magnify fontSize='small' />,
              endAdornment: (
                <IconButton
                  size='small'
                  title='clear'
                  aria-label='clear'
                  onClick={() => props.onMutationToolbarSearchValue('')}
                >
                  <Close fontSize='small' />
                </IconButton>
              )
            }}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}

export default UiDataGridToolbar
