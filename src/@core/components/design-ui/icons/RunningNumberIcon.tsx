import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const RunningNumberIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 24 24`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      className='UiSvgOutlined'
    >
      <path
        d='M13.9817 4.4691L12.0001 2.00004'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M19.0799 7.80068C20.2192 9.27994 20.8594 11.0968 20.8594 13.1062C20.8594 18.0029 16.9279 21.9344 12.0005 21.9344C7.07307 21.9344 3.14162 18.0029 3.14162 13.1062C3.14162 8.20948 7.07307 4.27703 12.0005 4.27703C13.2429 4.27703 14.4462 4.41913 13.9819 4.45649'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M13.9092 10.8363H10.8325L10.0803 13.1167H12.3678C13.2482 13.1167 13.9092 13.7777 13.9092 14.6581C13.9092 15.5385 13.2523 16.1995 12.3678 16.1995H10.0803'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default RunningNumberIcon
