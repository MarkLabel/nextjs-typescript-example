import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const MemberPermissionRoleIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 24 24`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      className='UiSvgOutlined'
    >
      <path
        d='M14.4369 19.0311L15.9496 20.5438L19.0169 17.5417'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M11.8383 10.8637C9.45923 10.8006 7.54862 8.83107 7.54862 6.43107C7.54119 3.99134 9.54731 2.00004 11.9765 2.00004C14.4057 2.00004 16.4118 3.99134 16.4118 6.43107C16.4118 8.83107 14.4934 10.8006 12.1134 10.8637C12.0366 10.8575 11.9496 10.8575 11.8383 10.8637Z'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M11.9765 21.8069C10.1712 21.8069 8.36482 21.3989 7.08865 20.3743C4.53108 18.8041 4.53108 16.1602 6.99445 14.5889C9.61466 12.9565 14.3583 12.9565 16.9585 14.5889'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default MemberPermissionRoleIcon
