import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const GlobalVariableIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 24 24`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      className='UiSvgOutlined'
    >
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M12.0006 21.9995C6.46493 21.9995 2.0006 17.5352 2.0006 11.9995C2.0006 6.46385 6.46493 2.00017 12.0006 2.00017C17.5363 2.00017 21.9996 6.46385 21.9996 11.9995C21.9996 17.5352 17.5363 21.9995 12.0006 21.9995Z'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M8.00048 3.0001H8.99947C7.0719 8.8315 7.0719 15.1675 8.99947 20.999H8.00048'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M15.0001 3.0001C16.9277 8.8315 16.9277 15.1675 15.0001 20.999'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M3.0001 16.0002V15.0002C8.83146 16.9277 15.1675 16.9277 20.999 15.0002V16.0002'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M3.0001 9.00017C8.83146 7.0716 15.1675 7.0716 20.999 9.00017'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default GlobalVariableIcon
