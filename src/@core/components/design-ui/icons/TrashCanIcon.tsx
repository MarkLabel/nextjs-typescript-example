import { useTheme } from '@mui/material/styles'

type SvgIconProps = {
  dimension?: number
  color?: string
}

const TrashCanIcon = (props: SvgIconProps) => {
  const theme = useTheme()
  const dimension = props.dimension ?? 24
  const color = props.color ?? theme.palette.grey[600]

  return (
    <svg
      width={dimension}
      height={dimension}
      viewBox={`0 0 24 24`}
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
      className='UiSvgOutlined'
    >
      <path
        d='M21.0003 6.0006C17.736 5.6799 14.3067 5.4997 11.0495 5.4997C9.0003 5.4997 6.9501 5.6549 4.8999 5.8006L3.0003 6.0006'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M8.49701 4.9986L8.74755 3.6796C8.90845 2.7286 8.99968 2.0007 10.7042 2.0007H13.2901C15.9946 2.0007 16.1101 2.7594 16.2587 3.6811L16.5032 4.9986'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M18.8572 9.1903L18.2496 19.2239C18.1444 20.8745 18.0711 22.0003 15.2067 22.0003H8.79329C6.92881 22.0003 6.85547 20.8745 6.75035 19.2239L6.14275 9.1903'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M10.3439 16.5631H13.6559'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M9.4987 12.5554H14.5013'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  )
}

export default TrashCanIcon
