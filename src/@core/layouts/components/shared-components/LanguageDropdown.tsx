// ** React Imports
import { Fragment, SyntheticEvent, useEffect, useState } from 'react'

// ** MUI Imports
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import IconButton from '@mui/material/IconButton'

// ** Icons Imports
import Translate from 'mdi-material-ui/Translate'

// ** Third Party Import
import { useTranslation } from 'react-i18next'
import Image from 'next/image'
// ** Type Import
import { Settings } from 'src/@core/context/settingsContext'

interface Props {
  settings: Settings
  saveSettings: (values: Settings) => void
}

const LanguageDropdown = ({ settings, saveSettings }: Props) => {
  // ** State
  const [anchorEl, setAnchorEl] = useState<any>(null)
  const [language, setLanguage] = useState<any>(null)

  // ** Hook
  const { i18n } = useTranslation()

  // ** Vars
  const { direction } = settings

  console.log()
  useEffect(() => {
    if (i18n.language === 'ar' && direction === 'ltr') {
      saveSettings({ ...settings, direction: 'ltr', language: i18n.language })
    } else if (i18n.language === 'ar' || direction === 'rtl') {
      saveSettings({ ...settings, direction: 'rtl', language: i18n.language })
    } else {
      saveSettings({ ...settings, direction: 'ltr', language: i18n.language })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [i18n.language, direction])

  const handleLangDropdownOpen = (event: SyntheticEvent) => {
    setAnchorEl(event.currentTarget)
  }

  const handleLang = (lang: string) => {
    const logoStyles = {
      width: '30px',
      height: '30px',
      display: 'inline',
      verticalAlign: 'middle',
      marginRight: '10px'
    }
    if (lang === 'th') {
      return (
        <div style={{ paddingTop: 5 }}>
          <Image
            width={30}
            height={30}
            src='/images/language/thailand.svg'
            alt='Change language'
          />
        </div>
      )
    } else if (lang === 'en') {
      return (
        <div style={{ paddingTop: 5 }}>
          <Image
            width={30}
            height={30}
            src='/images/language/english.svg'
            alt='Change language'
          />
        </div>
      )
    }
  }

  const logoDropDown = (lang: string) => {
    const logoStyles = {
      width: '30px',
      height: '30px',
      display: 'inline',
      verticalAlign: 'middle',
      marginRight: '10px'
    }
    if (lang === 'TH') {
      return <img src='/images/language/thailand.svg' style={logoStyles} />
    } else {
      return <img src='/images/language/english.svg' style={logoStyles} />
    }
  }

  const handleLangDropdownClose = () => {
    setAnchorEl(null)
  }

  const handleLangItemClick = (lang: 'en' | 'th') => {
    i18n.changeLanguage(lang)
    setLanguage('English')
    handleLangDropdownClose()
  }

  return (
    <Fragment>
      <IconButton
        color='inherit'
        aria-haspopup='true'
        aria-controls='customized-menu'
        onClick={handleLangDropdownOpen}
      >
        {handleLang(i18n.language)}
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleLangDropdownClose}
        sx={{ '& .MuiMenu-paper': { mt: 4, minWidth: 130 } }}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: direction === 'ltr' ? 'right' : 'left'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: direction === 'ltr' ? 'right' : 'left'
        }}
      >
        <MenuItem
          sx={{ py: 2 }}
          selected={i18n.language === 'th'}
          onClick={() => {
            handleLangItemClick('th')
            saveSettings({ ...settings, direction: 'ltr' })
          }}
        >
          <div>
            {logoDropDown('TH')}
            <p style={{ display: 'inline', fontSize: '1rem' }}>ภาษาไทย</p>
          </div>
        </MenuItem>
        <MenuItem
          sx={{ py: 2 }}
          selected={i18n.language === 'en'}
          onClick={() => {
            handleLangItemClick('en')
            saveSettings({ ...settings, direction: 'ltr' })
          }}
        >
          {logoDropDown('EN')}
          English
        </MenuItem>
      </Menu>
    </Fragment>
  )
}

export default LanguageDropdown
