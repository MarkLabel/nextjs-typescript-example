#!/bin/bash

set +x
nextFolder=".next"
export LC_ALL=C
source .env
ls -la
pwd
find $nextFolder \( -iname '*.js' -o -iname '*.json' \) 

while read line; do
  if [ "${line:0:1}" == "#" ] || [ "${line}" == "" ]; then
    continue
  fi
  
  key="$(cut -d'=' -f1 <<<"$line")"
  eval value=\$$key
  
  if [ -n "$key" ] && [ -n "$value" ]; then
    echo "Replace: APP_${key} with: ${value}"
    # find $nextFolder \( -iname '*.js' -o -iname '*.json' \) -type f -exec sed -i '' -e "s#APP_$key#$value#g" {} +;
    find "$nextFolder" -type f \( -iname '*.js' -o -iname '*.json' \) -print0 | xargs -0 sed -i -e "s#APP_$key#$value#g"
  fi
done < .env.production

echo "Starting Nextjs"
exec "$@"

